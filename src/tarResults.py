#!/usr/bin/python
""" This little script will create a bzip tar ball of data given a baseName,
extension, time step (dt), current time (T), and number of files to tar (N). N
is assumed to be all zeros except the leading digit.  
"""

import sys
import os
import tarfile as tf
import math
import glob

def getPattern(dt, T, stride):
    stepFinal = int(T/dt)
    stepInitial = int(T/dt) - stride

    digits = int(math.log10(stepFinal - 1)) + 1
    strideDigits = int(math.log10(stride))+ 1

    leadingDigits = int(stepInitial/10**strideDigits)

    if(leadingDigits > 0):
        diffStart = stepInitial - leadingDigits*10**strideDigits
        diffStop = stepFinal - leadingDigits*10**strideDigits - 1
        diffStart /= 10**(strideDigits-1)
        diffStop /= 10**(strideDigits-1)

        pattern = str(0)*(6 - digits) + str(leadingDigits) + \
            '[' + str(diffStart) + '-' + str(diffStop) + ']' + \
            '[0-9]'*(strideDigits-1)
    else :
        diffStart = int(stepInitial/10**(strideDigits - 1))
        diffStop = diffStart + int(stride/10**(strideDigits - 1)) - 1
        pattern = str(0)*(6 - strideDigits) + \
            '[' + str(diffStart) + '-' + str(diffStop) + ']' + \
            '[0-9]'*(strideDigits-1)

    return (pattern, stepInitial, stepFinal)

def compressAndRemove(baseName, ext, dt, T, stride):
    pattern, stepInitial, stepFinal = getPattern(dt, T, stride)

    files = baseName + pattern + '.' + ext 

    print('Creating tarball of files matching %s' % files)
    #create list of files to be tarred and deleted
    files = glob.glob(files)
    #create the tar ball
    tarName = baseName + str(stepInitial) + '-' + str(stepFinal - 1) + \
            '.tar.bz2'
    tarBall = tf.open(tarName, 'w:bz2')
    for f in files:
        tarBall.add(f)
        os.remove(f)

    tarBall.close()
    print('Created %s' % tarName)

def main(argv):
    if len(argv) < 6:
        sys.stderr.write('Usage: %s <baseName> <extension> <dt> <T> <stride>\n' % (argv[0],))
        return 1

    baseName = argv[1]
    ext = argv[2]
    dt = float(argv[3])
    T = float(argv[4])
    stride = int(argv[5])
    
    compressAndRemove(baseName,ext,dt,T,stride)

    print('Tarball complete.\n')
#    remove
    return 0

if(__name__ == "__main__"):
    sys.exit(main(sys.argv))
