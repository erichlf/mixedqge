__author__ = "Erich L Foster <efoster@bcamath.org>"
__date__ = "2013-08-27"

from solverbase import *

def f(f0,beta): #Coriolis parameter
    return Expression('f0 + beta*x[1]', f0=f0, beta=beta)

def Jac(psi, q):
    return psi.dx(1)*q.dx(0) - psi.dx(0)*q.dx(1)

class InitialConditions(Expression):
    def __init__(self, problem, Q, Psi):
        self.q0, self.psi0 = problem.initial_conditions(Q, Psi)
        self.q0 = project(self.q0,Q)
        self.psi0 = project(self.psi0,Psi)

    def eval(self, value, x):
        value[0] = self.q0(x)
        value[1] = self.psi0(x)

    def value_shape(self):
        return (2,)

class Solver(SolverBase):
#    Incremental pressure-correction scheme.

    def __init__(self, options):
        SolverBase.__init__(self, options)

    def solve(self, problem):
        #get problem mesh 
        mesh = problem.mesh

        t = 0 #initial time
        T = problem.T #final time
        dt = problem.dt #time step
        theta = problem.theta #time stepping method

        #problem parameters
        Re = problem.Re #Reynolds Number
        Ro = problem.Ro #Rossby Number

        # Define function spaces
        Q = FunctionSpace(mesh, 'CG', problem.Pq)
        Psi = FunctionSpace(mesh, 'CG', problem.Ppsi)
        W = Q * Psi 

        # Get boundary conditions
        bcs = problem.boundary_conditions(W.sub(0), W.sub(1), t)

        #define trial and test function
        p, chi = TestFunctions(W)

        w = Function(W)
        w_ = Function(W)

        #initial condition
        w = InitialConditions(problem, Q, Psi) 
        w = project(w, W)

        w_ = InitialConditions(problem, Q, Psi) 
        w_ = project(w_, W)

        q, psi = (w[0], w[1])
        q_, psi_ = (w_[0], w_[1])

        #q_(k+theta)
        q_theta = (1.0-theta)*q_ + theta*q

        #psi_(k+theta)
        psi_theta = (1.0-theta)*psi_ + theta*psi

        f = problem.F
        #weak form of the equations
        F = (1./dt)*(q - q_)*p*dx \
            + (1./Re)*inner(grad(q_theta),grad(p))*dx \
            + Jac(psi_theta,q_theta)*p*dx \
            - psi_theta.dx(0)*p*dx 
        F -= (theta*f(t)+ (1.0-theta)*f(t - dt))*p*dx
        F += q_theta*chi*dx - Ro*inner(grad(psi_theta),grad(chi))*dx

        # Time loop
        self.start_timing()

        #plot and save initial condition
        self.update(problem, t, w_.split()[0], w_.split()[1]) 

        while t<T:
            t += dt

            #evaluate bcs again (in case they are time-dependent)
            bcs = problem.boundary_conditions(W.sub(0), W.sub(1), t)

            solve(F==0, w, bcs=bcs)

            w_.vector()[:] = w.vector()

            q_ = w_.split()[0] 
            psi_ = w_.split()[1]

            # Update
            self.update(problem, t, q_, psi_)
        
        return q_, psi_

    def __str__(self):
          return 'QGE'
