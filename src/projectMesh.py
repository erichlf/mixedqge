#!/usr/bin/python
"""
Much of the following code was written by David R. Wells for QGE pack
and has been adapted to work in my FEniCS version of QGE.
"""

import re
import sys
import os
import numpy as np
from numpy import pi

def projection(match):
    node = int(match.group(1))
    x = float(match.group(2))
    y = float(match.group(3))
    z = float(match.group(4))

    [x_projected, y_projected] = miller_cylindrical([x,y,z], 0, 0)
    return [node, x_projected, y_projected]

def miller_cylindrical(coordinate_triples, longitude_offset=0,
                       latitude_offset=0):
    """
    A modified Mercador projection from geophysical coordinates to the
    plane.

    Required Arguments
    -------------------
    * coordinate_triples : 3-column, N row triples of sphere
      (cartesian) coordinates.

    Optional Arguments
    ------------------
    * latitude_offset : Offset in radians from the equator. Negative
      moves north, positive moves south.

    * longitude_offset : Offset in radians from Greenwich, England.
      Negative moves towards New York, positive moves towards Moscow.

    Output
    ------
    * Miller cylindrical projection (not scaled) of coordinate_triples.
    """
    latitudes, longitudes = cartesian_to_geographical(coordinate_triples)
    x_projected = longitudes - longitude_offset
    y_projected = 1.25*np.log(np.tan(pi/4 + 0.4*(latitudes - latitude_offset)))
    return np.array([x_projected, y_projected])


def cartesian_to_geographical(coordinate_triples):
    """
    Convert Cartesian coordinates to geophysical (latitude and longitude)
    coordinates.

    Required Arguments
    -------------------
    * coordinate_triples : 3-column, N row triples of sphere
    (cartesian) coordinates.

    Output
    ------
    * The tuple (latitudes, longitudes).
    """
    x = coordinate_triples[0]
    y = coordinate_triples[1]
    z = coordinate_triples[2]
    radius = np.sqrt(x**2 + y**2 + z**2)
    longitudes = np.arctan2(y, x)
    latitudes = np.arcsin(z/radius)
    return (latitudes, longitudes)

def main(argv):
    if len(argv) < 2:
        sys.stderr.write("Usage: %s <mesh file>" % (argv[0],)) 
        return 1

    if not os.path.exists(argv[1]):
        sys.stderr.write("ERROR: Mesh file %r was not found!" % (argv[1],))
        return 1

    meshFile = argv[1]
    f_in = open(meshFile, 'r')
    match = re.search('^(\w+)(\.msh)', meshFile)
    f_out = open(match.group(1)+'Projected'+match.group(2), 'w')

    pattern = re.compile('^(\d+)\s(-?\d+\.?\d*)\s(-?\d+\.?\d*)\s(-?\d+\.?\d*)\s*$')

    for line in f_in:
        match = pattern.search(line)
        if match:
          [node, x, y] = projection(match)
          f_out.write("%d %g %g %g\n" % (node, x, y, 0)) 
        else :
          f_out.write(line)
    return 0


if __name__ == "__main__":
    sys.exit(main(sys.argv))

