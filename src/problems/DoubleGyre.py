__author__ = "Erich L Foster <efoster@bcamath.org>"
__date__ = "2013-08-27"
#
#   adapted from channel.py in nsbench originally developed 
#   by Kent-Andre Mardal <kent-and@simula.no>
#

from problembase import *
from numpy import array

# No-slip boundary
class NoslipBoundary(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary 

# Problem definition
class Problem(ProblemBase):
#  Double gyre forcing 

    def __init__(self, options):
        ProblemBase.__init__(self, options)

        # Create mesh
        N = options["N"]
        self.mesh = RectangleMesh(0, -1, 1, 1, N, 2*N)

        self.Re = options["Re"] #Reynolds Number
        self.Ro = options["Ro"] #Rossby Number
        self.T = options["T"] #final time
        self.dt = options["dt"] #time-step
        self.theta = options["theta"] #theta for the theta time stepping method
        self.solver = options["linear_solver"] #what linear solver to use
        self.Pq = options["vorticity_order"] #order of vorticity element
        self.Ppsi = options["streamfunction_order"] #order of streamfunction element

    def initial_conditions(self, Q, Psi):
        q0 = Constant(0)
        psi0 = Constant(0)

        return q0, psi0

    def boundary_conditions(self, Q, Psi, t):
        # Create no-slip boundary condition for velocity
        noslipQ = DirichletBC(Q, 0, NoslipBoundary())
        noslipPsi = DirichletBC(Psi, 0, NoslipBoundary())

        bcs = [noslipQ, noslipPsi]

        return bcs

    def F(self, t):
        return Expression('sin(pi*x[1])', t=t) #Forcing function 

    def __str__(self):
        return 'Channel'
