__author__ = "Erich L Foster <efoster@bcamath.org>"
__date__ = "2013-08-27"
#
#   adapted from problembase.py in nsbench originally developed by
#   Anders Logg <logg@simula.no>
#

from dolfin import *
from math import *

class ProblemBase:
#   Base class for all problems.
    def __init__(self, options):

        # Store options
        self.options = options

        # Parameters must be defined by subclass
        self.mesh   = None
        self.bcq    = []
        self.bcpsi  = []
        self.Re     = None
        self.Ro     = None
        self.theta  = 0.5
        self.t      = 0
        self.T      = None
        self.dt     = None
        self.q0     = None
        self.psi0   = None
        self.q      = None
        self.psi    = None
        self.solver = None
        self.output_location = ''

    def update_problem(self, t, q, psi):
#        Update problem at time t

        # Update state
        self.t      = t
        self.q      = q
        self.psi    = psi

        # Call problem-specific update
        self.update(t, q, psi)

    def update(self, t, q, psi):
#        Problem-speficic update at time t
        pass
